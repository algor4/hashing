/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ktp.hashing;

import java.util.Scanner;

/**
 *
 * @author acer
 */
public class Hashing {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        LinearProblingHashTable<String, Integer> st = new LinearProblingHashTable<String, Integer>();
        String pcode;
        int price;
        while (true) {
            pcode = kb.next();
            if (pcode.equals("*")) {
                break;
            }
            price = kb.nextInt();
            st.put(pcode, price);
        }

        String num = kb.next();
        if (st.get(num) == null) {
            System.out.println("not found");
        } else {
            System.out.println(st.get(num));
        }
    }
}

class LinearProblingHashTable<Key, Value> {

    private int M = 100;
    private Value[] vals = (Value[]) new Object[M];
    private Key[] keys = (Key[]) new Object[M];

    private int hash(Key key) {
        return (key.hashCode() & 0x7FFFFFFF) % M;
    }

    public void put(Key key, Value value) {
        int i;
        for (i = hash(key); keys[i] != null; i = (i + 1) % M) {
            if (keys[i].equals(key)) {
                break;
            }
        }
        keys[i] = key;
        vals[i] = value;
    }

    public Value get(Key key) {
        for (int i = hash(key); keys[i] != null; i = (i + 1) % M) {
            if (keys[i].equals(key)) {
                return vals[i];
            }
        }

        return null;

    }

}

////// test1 
////025441 520
////002451 690
////563244 799
////005786 850
////142110 460
////*
////002451
//ans 690

//test2
//025441 520
//002451 690
//563244 799
//005786 850
//142110 460
//*
//123456
//not found